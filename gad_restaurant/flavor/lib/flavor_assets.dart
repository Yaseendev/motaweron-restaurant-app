class AppImages {
  static const String ROOTURI = 'flavor/assets/images/';
  static const String OFFER = ROOTURI + 'offer.jpg'; 
  static const String APPLOGO = ROOTURI + 'gadLogo.png';
  static const String PHOTO_SHOW1 = ROOTURI + 'ad1.jpg';
  static const String PHOTO_SHOW2 = ROOTURI + 'ad2.jpg';
  static const String PHOTO_SHOW3 = ROOTURI + 'ad3.jpg';
  static const String PHOTO_SHOW4 = ROOTURI + 'ad4.jpg';
  static const String RESTAURANT_HOME_CARD_PIC = ROOTURI + 'gadLogo.png';
  static const String TRANSAPPLOGO = ROOTURI + 'gadLogo.png';
  static const String MAP_PIN = ROOTURI + 'pin.png';
  static const String COVER = ROOTURI + 'cover.png';
}
